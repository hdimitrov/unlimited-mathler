
interface UsedGuesses {
    guess: string[];
    guessColours: string[];
};

interface AnswerMapEntry {
    index: number;
    correctLocation: number;
    type: 'green'|'yellow';
}

interface CheckedAnswer {
    map: AnswerMapEntry[];
    index: number;
    score: number;
}

interface UsedLetter {
    letter: string;
    type: 'yellow'|'green'|'grey';
    index: number;
}