const putInCache = async (request, response) => {
    const cache = await caches.open("v1");
    await cache.put(request, response);
};

const cacheFirst = async (request) => {
    if(navigator.onLine) {
        const responseFromServer = await fetch(request);
        await putInCache(request, responseFromServer.clone());
        return responseFromServer;
    }
    const responseFromCache = await caches.match(request);

    if (responseFromCache) {
        return responseFromCache;
    }
    const responseFromNetwork = await fetch(request);
    putInCache(request, responseFromNetwork.clone());
    return responseFromNetwork;
};

self.addEventListener("fetch", (event) => {
    event.respondWith(cacheFirst(event.request));
});
